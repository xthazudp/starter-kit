import React from 'react';
import AppRouting from './AppRouting';

const App = (args) => {
  return (
    <div>
      <AppRouting />
    </div>
  );
};

export default App;
